from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from ...utils import CentrifugoMessage


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--content', dest='content', default='', help='Content')

    def handle(self, *args, **options):
        message = CentrifugoMessage(content=options.get('content'))
        message.send()

