import json
import requests

from django.conf import settings

from hashlib import sha256
import hmac


def generate_api_sign(secret, encoded_data):
    """
    Generate HMAC SHA-256 sign for API request.
    @param secret: Centrifugo secret key
    @param encoded_data: json encoded data to send
    """
    sign = hmac.new(str(secret).encode('latin1'), digestmod=sha256)
    sign.update(encoded_data.encode('latin1'))
    return sign.hexdigest()


class CentrifugoMessage:
    def __init__(self, content):
        self.content = content
        self.command = {
            "method": "publish",
            "params": {
                "channel": "news",
                "data": {
                    "content": self.content
                }
            }
        }
        self.api_key = generate_api_sign(settings.CENTRIFUGE_SECRET, encoded_data=json.dumps(self.command))

    def send(self):
        data = json.dumps(self.command)
        headers = {'Content-type': 'application/json', 'Authorization': 'apikey ' + settings.CENTRIFUGE_SECRET}
        response = requests.post(
            url='http://localhost:8000/api', data=data, headers=headers
        )
        print(response.json())
